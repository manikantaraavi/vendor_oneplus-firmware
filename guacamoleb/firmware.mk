LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),guacamoleb)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
    LOGO \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hyp \
    keymaster \
    modem \
    qupfw \
    storsec \
    tz \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

endif

